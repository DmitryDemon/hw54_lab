import React, { Component } from 'react';
import './App.css';
import Card from './components/Card/Cards';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Card/>
      </div>
    );
  }
}

export default App;
