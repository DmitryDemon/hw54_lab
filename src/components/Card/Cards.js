import React, { Component, Fragment } from 'react';
import './Cards.css'
import CardDeck from "./CardDeck";
import Card from './Card';
import PokerHand from "./PokerHand";


class Cards extends Component {

    state = {
        cards: [],
        result: ''
    };

    getCardsInHand = () => {
        let cards = new CardDeck().getCards(5);
        this.setState({cards});
    };

    getHandCard = () => {
        return new PokerHand().getOutcome(this.state.cards);
    };


    render(){
        return(
            <Fragment>
                <p>{this.getHandCard()}</p>
                <Card entries={this.state.cards}/>
                <div>
                    <button onClick={this.getCardsInHand}>Shuffle Cards</button>
                </div>
            </Fragment>
        )
    }

}
export default Cards;