import React from 'react';
import './Cards.css';

const Card = (props) =>  {

    return (
        props.entries ? props.entries.map((card, id) => {
            return (
                    <div key={id} className={"Card Card-rank-" + card.ranks.toLowerCase() +" Card-" + card.name + " "}>
                        <span className="Card-rank">{card.ranks}</span>
                        <span className="Card-suit">{card.suits}</span>
                    </div>
            )
        }) : null
    )

};

export default Card;

