import './PokerHand';
import PokerHand from "./PokerHand";
class CardDeck  {
    constructor() {
        this.suits = ["C", "D", "H", "S"];
        this.ranks = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
        this.deck = [];


        this.getDeck = () => {
            for (let i = 0; i < this.suits.length; i++) {
                for (let j = 0; j < this.ranks.length; j++){
                    let card = {
                        suits: this.suits[i],
                        ranks: this.ranks[j],
                        name: ''
                    };
                    this.deck.push(card);
                }
            }
        };
        this.getDeck();


        this.suitsUni = {
            "D":{unicode: '♦', name: 'diams', id: 'D'},
            "H":{unicode: '♥', name: 'hearts', id: 'H'},
            "C":{unicode: '♣', name: 'clubs', id: 'C'},
            "S":{unicode: '♠', name: 'spades', id: 'S'}
        };



        this.getCard = () => {
           let random = Math.floor(Math.random() * this.deck.length);
           const randCard = this.deck[random];
            this.deck.splice(random, 1);
            return randCard;
        };



        this.getCards = (howMany) => {
            let arrCards = [];
            for (let i = 0; i < howMany; i++){
              arrCards.push(this.getCard()) ;
            }
            for (let i = 0; i < arrCards.length; i++){
                if (arrCards[i].suits === this.suitsUni[arrCards[i].suits].id) {
                    arrCards[i].name = this.suitsUni[arrCards[i].suits].name;
                    arrCards[i].suits = this.suitsUni[arrCards[i].suits].unicode;
                }
            }
            this.pokerHand = new PokerHand().getOutcome(arrCards);
            return arrCards;

        };


    }
}
export default CardDeck;