class PokerHand {
    constructor(){
        this.getOutcome = (arrCards) =>{
            let t = 0;
            let f = 0;

            for (let i = 0;i < arrCards.length;i++){
                for (let j = i+1;j < arrCards.length;j++){
                    if (arrCards[i].suits === arrCards[j].suits){
                        f++;
                    }
                    if (arrCards[i].ranks === arrCards[j].ranks){
                        t++;
                    }
                }
            }
            if (f === 10) return "combination is FLUSH";
            if (t === 3) return 'combination is SET';
            if (t === 2) return 'combination is TWO PAIR';
            if (t === 1) return 'combination is PAIR';
            if(t === 0) return 'combination is NONE';
        }
    }
}
export default PokerHand;


